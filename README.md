# Minor Project: Training a deformed wing virus basecalling model with taiyaki

![Version](https://img.shields.io/badge/version-2.0-blue.svg?cacheSeconds=2592000)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0.html)

> This repository provides a pipeline for basecalling with taiyaki.

## Table of contents

[TOC]

---
## Introduction

In recent years, the populations of the Western honeybee (Apis mellifera) have been declining drastically.  One of the main causes is the increase of bees infected with the Deformed wing virus (DWV; Iflaviridae). When this virus enters a host, it will change the structure of the wings. Bees infected with this virus are not able to fly because their wings are shriveled and deformed. The virus is spread by the Varroa destructor mite. The data for this project was sequenced using the Oxford Nanopore Technologies MinION sequencer and an R9 flow cell. This sequencer produces long reads with a relatively high accuracy at a low cost. Long reads are extremely valuable because they provide information on how distal sequences are spatially related. However, the MinION sequencer has a notable error rate, somewhere between the five and fifteen percent. The MinION sequencer contains a R9 flow cell with proteins, also called pores. Genetic data flows through the pore and measures an electric signal. The strength of the signal is based on the nucleotides. MinION reads need to be base called in order to know the nucleotide sequence. The aim of this study is to counter the error rate of the MinION sequencer with a base-caller trained on the DWV data and to improve this base-calling model further. This training is done with a deep learning model. When this goal is achieved, we can answer questions about the mutation rate.

Two snakemake pipelines were developed, one for training a new basecalling model and the other for comparing the new model with one of guppy's model. A consensus tool was developed to create an MSA of the basecalled reads.

---
## Dependencies


- A computer with GPU is neccessary for training with taiyaki.

---
## Installation

We created a Docker container in collaboration with two other students from the Hanze Hogeschool. This container has our pipelines and consensus tool embedded. It can be found in the [Bumblebee repository](https://bitbucket.org/ajdrost/bumblebee/src/master/). By installing this repository you will have all the tools and dependencies needed for the pipelines and consensus tool imeadiately installed within the Docker and the following steps can be skipped.

> It is recommended to install the tools in a self-contained [virtual environment](https://docs.python.org/3/tutorial/venv.html).

* Install Guppy

Guppy is a basecalling software from Oxford Nanopore Technologies. Install it with the following commands:

```sh
# Download and unzip the compressed Guppy repository from Oxford Nanopore Technologies.
wget https://mirror.oxfordnanoportal.com/software/analysis/ont-guppy_3.4.4_linux64.tar.gz | tar zxvf ont-guppy_3.4.4_linux64.tar.gz
```
* Install minimap2

Minimap2 is a versatile sequence alignment program that aligns DNA or RNA sequences against a large reference sequence. It performs especially well on long reads, for example reads from either PacBio or Minion Oxford Nanopore Technologies.

```sh
# Download and unzip the compressed github repository.
curl -L https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-2.17_x64-linux.tar.bz2 | tar -jxvf -
./minimap2-2.17_x64-linux/minimap2
```

* Install taiyaki

Taiyaki can be installed by cloning their repository. Look at the [taiyaki github](https://github.com/nanoporetech/taiyaki) for other ways to install taiyaki.

```bash
# Use pip to install taiyaki in a virtual env
pip install path/to/taiyaki/repo
pip install -e path/to/taiyaki/repo #[development mode](http://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode)
```

* Install Snakemake

The pipeline is written with snakemake. Install snakemake to execute the pipeline.

```sh
# Install snakemake
pip install snakemake
```

It is not possible to create a pipeline image if dot is not installed. When running the snakemake --dag command and dot is not installed, it will return a Broken Pipe error. Please install graphviz with the following command if this happens.

```sh
# Install graphviz
apt-get install graphviz
```

* Install Nanoplot

Nanoplot is a tool that creates plots based on a basecalling summary file. The plots visualize the read length and base quality of the different reads. Use the following command to install Nanoplot:

```sh
    pip install NanoPlot
```

* Install Clustal Omega

Clustal Omega is a tool for creating a multiple sequence alignment. Use the following command to install Clustal Omega:

```sh
conda install -c bioconda clustalo
# OR
conda install -c bioconda/label/cf201901 clustalo
```

* Required R libraries

```r
library(dyplr)
library(ggplot2)
# Anderson Darling test
library(nortest)
```

---
## Usage and Features

### Snakemake pipelines

Two snakemake pipelines were developed, they can be found in the taiyaki directory. One for training a new basecalling model (trainings pipeline) and the other for comparing the new model with a guppy model (basecall pipeline).

#### The Taiyaki Trainings Pipeline

This pipeline uses Taiyaki to train a new basecalling model. The configuration of this pipeline is based on RNA Deformed Wing Virus reads from Nanopore. The process/log of obtaining this configuration can be found in our [wiki](https://bitbucket.org/jfhaaijer/beevirus1/wiki/DWV%20basecalling%20model%20configuration%20log). 
 When all the necessary tools are installed, the pipeline can be run with the following command:

```sh
# Execute taiyaki pipeline
snakemake --cores all
# --cores all specifies the number of cores to use, in this case all of the cores are used.
```

The steps of the pipeline can be divided into three stages: Preparation, Training and Evaluation. The pipeline is shown in Figure 1 and has the following steps:

1. Preparation
    -  Basecall (basecall)
    -  Alignment (map_basecall)
    -  Prepare mapped reads (create_scaling_params, extract_references & create_mapped_read_file)
2. Training
    -  Training (train_model & export_to_guppy)
    - Basecalling with the new model (basecall_with_new_model)
    - Align basecalled reads against consensus (Align with Guppy)
    - Calibrate Q-scores (calibrate_qscores)
3. Evaluation
    -  Basecall with adjusted Q-scores (basecall_final)
    -  Evaluation (quality_check, plot_mapped_reads, & plot_training)

![alt text](https://trello-attachments.s3.amazonaws.com/5e415bfb05c887724b30a712/5ea449ca3ced636b5186fd5e/f7a2a427930474ddb56f148b3fd9f3b4/taiyaki_pipeline.png)

##### 1. Preparation

###### 1.1 Basecall with Guppy

The first step is base calling with Guppy and can be found in the pipeline as rule basecall. Guppy requires a directory of fast5 files and a configuration model. The configuration models can be found in data directory of the ont-guppy project. It has DNA and RNA configuration models for reads sequenced with R9.4.1 nanopores, each model has a high accuracy version and a fast version. The output is a directory containing the fastq files, two summary files and a log file.

```sh
guppy_basecaller -i reads -s basecalls -c rna_r9.4.1_120bps_hac.cfg --device cuda:0
```

| Options                         | Description                                   |
| --------------------------------|-----------------------------------------------|
| -i reads                        | An input directory with fast5 files           |
| -s basecalls                    | The output directory for the basecalled reads |
| -c rna_r9.4.1_120bps_hac.cfg    | The guppy configuration model, this one is specifically for RNA. One of the options in this configuration replaces the U with a T|
| --device cuda:0                 | Select one of the GPU's with CUDA              |

###### 1.2 Alignment with Minimap2

The generated fastq files from the previous step and the DWV reference genome acquired from NCBI are used by Minimap2 in rule map_basecall. This tool aligns each read to the reference genome to determine the specific reference fragment for each read. The resulting alignment in SAM format is converted to a BAM file with SAMtools. BAM files are the numeric version of SAM files and occupy less disk space.

```sh
 Minimap2 -I 16G -ax map-ont -t 32 -a --secondary=no reference.fasta basecalls/*.fastq | samtools view -b -S -T reference.fasta - > basecalls.bam
```

| Minimap Options   | Description                                   |
| ------------------|--------------------------------------------------------------------|
| -I 16G            | Only split the index every 16 gigabases                            |
| -ax map-ont       | Preset for mapping noisy RNA ONT reads to a reference              |
| -t 32             | Use 32 threads to run                                              |
| -a                | Output in SAM format                                               |
| --secondary=no    | Do not output secondary alignments                                 |
| reference.fasta   | Fasta file format containing the reference sequence to map against |
| basecalls/*fastq  | The fastq files output from the basecall                           |

###### 1.3 Prepare mapped reads

###### 1.3.1 Extract References
 
    This step extracts the reference sequence for each read with the get_refs_from_sam.py script from Taiyaki. This step is executed by the rule extract_references. It requires alignment output from the previous step and the DWV reference genome. The reference sequences obtained with this script are used as the "true sequences" for the training. The default genetic data for Taiyaki is DNA. However, the data that is used for this project is RNA. During DNA sequencing, the strands of DNA go through the pore starting at the 5' end of the molecule. In contrast, during direct RNA sequencing the strands go through the pore starting at the 3' end. As a consequence, the per-read reference sequences used for RNA training must be reversed with respect to the genome/exome reference sequence. Therefore, the –reverse parameter is added.

```sh
    get_refs_from_sam.py reference.fasta  --reverse --min_coverage 0.8 > read_references.fasta
    # Add reverse when working with RNA
    # Select only reads with a coverage higher than 80%
```

###### 1.3.2 Create scaling parameters
    
     This step selects how much each read is trimmed and it creates scaling parameters for each read with the generate_per_read_params.py script from taiyaki. This step is executed by the rule create_scaling_params. It uses the directory with the fast5 files to generate a tab separated file (tsv) with the following columns UUID (Read ID), trim_start, trim_end, shift and scale.

```sh
    generate_per_read_params.py --jobs 32 reads > read_params.tsv
    # Jobs is the number of threads
```

###### 1.3.3 Create a mapped read file

    This step uses the sample directory with fast5 files, the base model and output from [create_scaling_params] and [extract_references]. This step is excecuted by the rule create_mapped_read_file. The base model is used to create an alignment between raw signal and reference sequence. The taiyaki script combines them into a single file, with for each read the parameters, the reference ("true sequence"), the raw signal and an alignment between the raw signal and the reference.  

```sh
    prepare_mapped_reads.py --jobs 32 reads read_params.tsv mapped_read.hdf5 r941_rna_minion.checkpoint read_references.fasta
```

| Options                    | Description                                         |
| -------------------------- | --------------------------------------------------- |
| reads                      | A directory containing the raw fast5 reads |
| read_params.tsv            | Tab seperated file with per read scaling and trimming parameters |
| mapped_read.hdf5           | The output, a hdf format file with the signal, reference and parameters for each read |
| r941_rna_minion.checkpoint | The model that aligns the raw signal with the reference |
| read_references.fasta      | The reference sequence per read |

##### 2. Training

###### 2.1 Train a model

The progress is displayed on the screen and written to a log file in the training directory. Checkpoints are regularly saved and training can be restarted from a checkpoint by replacing the model description file with the checkpoint file on the command line.

```sh
    train_flipflop.py --device cuda:0 --overwrite --stride 10 --winlen 31 --seed 2 r941_rna_minion.checkpoint mapped_reads.hdf5 --outdir training
```

| Options                    | Description                                         |
| -------------------------- | --------------------------------------------------- |
| --device cuda:0            | Select which GPU with cuda |
| --overwrite                | Overwrite the training if the output directory already exists |
| --stride 10                | The output, a hdf format file with the signal, reference and parameters for each read |
| --winlen 31                | The model that aligns the raw signal with the reference |
| --seed 2                   | The seed makes this project reproducible |
| r941_rna_minion.checkpoint | Model definition file |
| mapped_reads.hdf5          | A hdf format file with the signal, reference and parameters for each read |
| --outdir training          | The name of the directory where the checkpoint files and the resulting model wil be stored |

###### 2.2 Export to guppy

Convert the final checkpoint file from the training into a json file with taiyaki script dump_json.py. Guppy requires json files for its basecalling.

```sh
    dump_json.py training/model_final.checkpoint > 'pwd'/model.json
```

###### 2.3 Basecall with new model

Basecall the files in the fast5 directory again but with the re-trained model. It is basically the same as step 1, but a model files is specified by the -m option.

```sh
guppy_basecaller -i samples -s basecalls -c rna_r9.4.1_120bps_hac.cfg -m 'pwd'/model.json --device 0
```

| Options                         | Description                                   |
| --------------------------------|-----------------------------------------------|
| -i samples                        | An input directory with fast5 files           |
| -s basecalls                    | The output directory for the basecalled reads |
| -c rna_r9.4.1_120bps_hac.cfg    | The guppy configuration model, this one is specifically for RNA. One of the options in this configuration replaces the U with a T|
| 'pwd'/model.json                | Use the retrained model to basecall           |
| --device cuda:0                 | Select one of the GPU's with CUDA             |

###### 2.4 Align with Guppy

The basecalled reads are then aligned with the refercen that was used for training. The alignment summary will be copied to the directory containing the basecalled reads (2.3)

```sh
guppy_aligner -i samples -s guppy_alignment --align_ref reference.fasta && cp guppy_alignment/alignment_summary.txt samples
```

| Options                         | Description                                   |
| --------------------------------|-----------------------------------------------|
| -i samples                        | An input directory with fast5 files           |
| -s guppy_alignment                    | The output directory for the alignment summary |
| reference.fasta               | reference genome in fasta format         |

###### 2.5 Calibrate Qscores

The Q-score scale and offset will be adjusted based on the alignment accuracy achieved in 2.4.

```sh
taiyaki/misc/calibrate_qscores_byread.py --input_directory samples --plot_filename results/qscore_calibration.png 1> qscores.log
```

##### 3. Evaluation

###### 3.1 Basecall with adjusted Q-scores

Set the Qscore scale and offset parameters from guppy to the ones callibrate by the previous step.

```sh
guppy_basecaller -i reads -s basecalled_reads_final -c ont-guppy/data/rna_r9.4.1_70bps_hac.cfg --qscore_offset -0.88 --qscore_scale 1.25 -m pwd/model.json --device cuda:0"
```

###### 3.2 Quality check with Nanoplot

    The quality and the read length of each read can be observed by Nanoplot. This tool creates a lot of different graphs to visualize the performance of the basecalling.

```sh
    Nanoplot --summary basecalls/sequencing_summary.txt --loglength -o Nanoplot_output_basecalls --N50 --raw
```

| Options                                    | Description                                   |
| ------------------------------------------ | --------------------------------------------- |
| --summary basecalls/sequencing_summary.txt | A text file format summary of the basecalling performance |
| --loglength                                | Create plots that are log transformed|
| -o Nanoplot_output_basecalls               | The output directory |

###### 3.3 Plot mapped reads

    A mapped read file is created in step 3 prepare mapped reads. It contains the parameters, reference and singal per read. It also contains an alginment between the signal and the reference. This alignment is visualised in a mapped reads plot. The plot_mapped_singals.py script was written by taiyaki and can be found in the misc directory of taiyaki. Examples of the output can be found in the taiyaki/output/ directory on this repository.

```sh
    plot_mapped_singals.py mapped_reads.hdf5 --nreads 100 --output plot_mapped_reads.png

```

| Options                                    | Description                                   |
| ------------------------------------------ | --------------------------------------------- |
| --nreads 100                               | The number of reads used for the plot |
| --output plot_mapped_reads.png             | The output file with a png format |

###### 3.4 Plot training

    The training is also visualized in a graph, it shows the training loss. The script to create this plot can be found in the taiyaki misc directory. Examples of the output can be found in the taiyaki/output/ directory on this repository.

```sh
    plot_training.py plot_training.png training
```

###### 3.5 Create a workflow image

    This rule creates an image of the pipeline. An example of this is Figure 1 from this readme.

```sh
    snakemake --dag | dot Tpng > snakemake_pipleine.png
```

#### The Taiyaki Basecall Pipeline

This pipeline was created with snakemake to compare the new model with the guppy model. First it basecalls the reads with both models (A txt file with read ID's can be used to filter reads in this step). Then the basecalled reads are aligned with a reference model. Both the alignment and basecalled reads are visualized with NanoPlot. The commands of these steps are similar to those in trainings pipeline.

 ![alt text](https://trello-attachments.s3.amazonaws.com/5e415bfb05c887724b30a712/5ec3b7da15776873f45fddab/8abe2a96ef10601462af2518286f4d80/basecall.png)

#### Create a consensus sequence

The best way to validate your taiyaki for errors is to use a consensus sequence as reference sequence.
We have made a program that makes a consensus sequence.

##### Snakefile

The snakefile makes a MSA from the input files and creates a consensus sequence of it. The snakefile tool is not compatible with the iteration function so it could only do one run.

snakemake --snakefile Snakefile

##### msa_consensus_tool.py

The msa_consensus_tool makes use of two other scripts: validate_msa.py and multi_fasta.py. The two scripts could be used seperately from the msa_consensus_tool. The execution of this tool could take multiple hours or days depending if the iteration function is used and how big the starting multiple_fasta file is.

To use the msa_consensus_tool type:
python msa_consensus_tool.py --inputfile=absolute_path_to_your_file.fasta --outputdir=absolute_path_to_your_output_directory/

| argument | example | explanation |
|------------------|--------------------------------------------------------------|--------------------------------------------------|
| inputfile | /data/all_world.fasta | Input must be a multi fasta file |
| outputdir | /output_dir/ | output directory |
| quickway  | t | if quickway is true up to top five worst scoring accession codes could be removed for the msa at once |
| iteration | t | if iteration is true it will look if there are any accession codes which lie above their calculated threshold and remove them. It will stop iteration when there are no more sequences above the threshold. |
| maxiteration| 10 | Iteration will continue until 10 iteration are completed or if there are no sequences above their respective thresholds. |
| secondinputfile| /data/data2.fasta | If you want to easily compare the MSA of only reference used with the basecalled reads. |
| types| rna | Uses U instead of T for all calculations about nucleotides. |
| last_session| yes | To continue where the last session left of and the last completed iteration. |

##### multi_fasta.py

The multi_fasta.py transforms all sequences in multi_fasta to rna and if desired could merge two different multi_fasta files en writes it into one fasta file.

To execute the multi_fasta.py type:
python multi_fasta.py --inputfile=yourfile.fasta --outputfile=youroutputfile.fasta

| argument | example | explaination |
|------------------|--------------------------------------------------------------|--------------------------------------------------|
| inputfile | /data/all_world.fasta | Needs a mutliple fasta file as input |
| outputfile | /output/multi_fasta.fasta | The output of the script will be written to this file |
| secondinputfile |/output/multi_fasta2.fasta  | Use of a seconde mulitple fasta file is optional |
| types | rna | will transform every sequence from the input file(s) to this type of genome data. |

##### validate_msa.py

Validate_msa.py take a msa output in the form of a fasta file and calculates scores of the MSA. It also makes visualization about gap insertion and extension and the most frequent nucleotide per position in percentages. This makes it easier to determine if the MSA is of good quality and doesn't need further improvements.

To execute the validate_msa.py type:


| argument | example | explaination |
|---------------------|-----------------------------------------------------------------------|---------------------------------------------------------|
| inputfile | /msa.output.fa | A fasta file that is generated from executing a clustal o   |
| outputdir | /outputdir/ | output directory where a msa_visualization directory, bad_accession.txt and Temp directory wil be created. All the plots from the script validate_msa.py will be put into msa_visualization directory. bad_accession.txt will be put in outputdir and Temp directory will be placed in outputdir but will be automatically removed |
| quickway | t | To remove up to the quickway_limit (which is by default 5) of the worst scoring accession codes at a time. |
| type | rna | For counting certain nucleotides which are not present in dna |
| quickway_limit | 5 | If quickway is set to true the user can determine the amount of bad accession numbers will be removed from the MSA. |

##### oo_consensus.py

oo_consensus.py creates a consensus from a MSA fasta output by taking the most frequent nucleoted per position and use it for the consensus sequence. By default there can only be 20% gaps per position to meet the requirements. It makes additional visualizations plot for the MSA like Mutations regio's. The script notes ambigious letter and if the nucleotide for that position changes after having the ambigious letter processed. It also notes where and when there are multiple consecutive sequences that have a coverage of 100% ergo all sequences have the same nucleotides for those positions.

The consensus program writes three output reports: 

- correct_sequences.txt:  When the nucleotide on a certain position is chosen by all sequences five or more times in a row. Contains the sequence with their the start and stop position and sequence length 

- Nuc_changes.txt: Each time the involvement of a non-nucleotide letter changes the outcome or doesnt change the outcome, if there are more nucleotides with the same top frequency on that position and when it has to choose randomly between the letters.

- Info_bout_consensus.txt: Contains how long the MSA was, how many nucleotides all sequences agree on on the same position, how many times nothing was added to the consensus sequence because it didnt met the requirements, the length of the consensus sequence, the frequency of coverage for every nucleotide chosen for the consensus sequence between 0 and 100% in per 10%, the percetage of the consensus sequence that has a coverage of more than 50%, the characters that the script can't handle.

To execute the oo_consensus.py type:
python oo_consensus.py --input_file=msa_output_file.fa --output_file=/output/consensus.fasta --report_dir="/output_dir/"

| argument | example | explaination |
|---------------------|-----------------------------------------------------------------------|---------------------------------------------------------|
| inputfile | /0_iteration/msa_output.fa | Is the output of a MSA in fasta file form. |
| outputfile | /0_iteration/Your_own_consensus.fasta | The place where the consensus sequence file will be placed |
| reportdir | /0_iteration/ | A directory will be created here with the name "consensus_evaluation/", all of the output except the consensus sequence file itself will be placed in this newly created directory (consensus_evaluation/). |
| perfectlength | 20 | Set the minimum amount of [counts]% coverage consecutive position for it to be noted into correct_sequences.txt |
| counts | 100 | how many percent coverage per position must there be for it to maybe be put into correct_sequences.txt  |
| type | rna | For counting certain nucleotides which are not present in dna  |
| header | my_consensus_name | header for the consensus sequence  |
| maximum_gaps | 0.2 | maximum percentage of gaps that will be allowed. between 0.0 and 1.0 |

---

## Suggestions for future updates

- Make it easier to train a model on multiple datasets (trainings pipeline)
- Use multi-processing to decrease the time it takes to create a consensus/msa with the consensus tool
- Distribute the taiyaki pipeline tasks between CPU and GPU, to increase the speed.
- Merging plots created by the consensus tool for easier comparison

---

## Bugs

---

## Contacts

👤 **Jippe Silvius & Jildou Haaijer**

For any problems or questions:

**j.d.silvius@st.hanze.nl & j.f.haaijer@st.hanze.nl**

For questions about the consensus/msa plots: Mail Jippe at j.d.silvius@st.hanze.nl

For further quetions about the taiyaki snakemake pipeline: Mail Jildou at j.f.haaijer@st.hanze.nl

---

## Licenses

This project is [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.
