configfile: "basecall_config.yml"
container: "docker://continuumio/miniconda3:4.4.10"

"""
This is a snakemake pipeline used for basecalling Minion Nanopore reads, 
The following files are required, their paths should be changed in the basecall_config.yml file.
- Fast5 sample directory
- Read_ids.txt: file with the read ids to filter out
- Reference genome, in our case this is a consensus sequence of DWV genomes
- The guppy config model: for example rna_r9.4.1_70bps_hac.cfg (for RNA R9 flowcell)
- The re-trained taiyaki model: model.json
- The path of the guppy basecaller

Prerequisites:
- Nanoplot needs to be installed for Quality control
- Minimap2 needs to be installed for the alignment
- Guppy needs to be installed for basecalling

This pipeline basecalls with the original guppy model and with the retrained taiyaki model.
A quality assesment is done with Nanoplot for both models.

Run with: snakemake --snakefile basecall.smk --cores all
(install snakemake with: pip install snakemake)
"""

rule all:
    input:
       expand(["nanoplot_{version}/basecalls", "nanoplot_{version}/alignment"], version = ["guppy", "taiyaki"]),
      # expand(, version = ["guppy", "taiyaki"]),

## Step 1: Basecall the reads with ONT Guppy
rule basecall:
    """Basecall the fast5 reads with the Guppy basecaller from Oxford Nanopore Technologies."""
    input:
        samples = config["fast5_sample_dir"],   # fast5 sample dir
        id_list = config["read_ids"],   # Read ids to filter
        config = config["config_model"], # Guppy config
        model = config["basecall_model"], # Basecall model
        basecaller = config["basecaller"]  # Guppy basecaller
    params:
        scale = 9.88,   
        offset = -59.85,
        run = '{version}'
    output:
        dir = directory("basecalled_reads/{version}") # output dir
    message:
        "Basecalling reads from: {input.samples} with model: {input.model}, qscale = {params.scale}, qoffset = {params.offset}. Ouput stored in output:{output}"
    run:
        if params.run == "guppy":
            shell("{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config} --device cuda:0 --read_id_list {input.id_list}")
        if params.run == "taiyaki":
            shell("{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config} -m {input.model} --qscore_offset {params.offset} --qscore_scale {params.scale} --device cuda:0 --read_id_list {input.id_list}")
        #  #/home/tools/ont-guppy/bin/guppy_basecaller -i /home/reads_test1 -s basecalls_test --qscore_offset {params.offset} --qscore_scale {params.scale} -c /home/tools/ont-guppy/data/rna_r9.4.1_70bps_hac.cfg --device cuda:0

# Step 2: Align with minimap2
rule align_with_minimap2:
    """Align the basecalled reads with the reference to see how good it aligns"""
    input:
        basecall_dir = "basecalled_reads" + "/{version}",  # Basecalled reads dir for guppy and taiyaki model
        minimap2 = config["minimap2"],  # Path to the minimap2 tool
        reference = config["reference_genome"]  # Reference Genome (consensus)
    output:
        alignment = "alignment/{version}/basecalls.bam"  # Sorted Alignment
    params:
        dir = "alignment/{version}"  # Ouput dir name
    message:
        "This method creates an alignment with Minimap2. For both {version} basecalled reads. output = {output.alignment}."
    shell:
        """mkdir -p {params.dir} 
        {input.minimap2} -d {params.dir}/ref_index.mmi {input.reference} 
        {input.minimap2} -a {params.dir}/ref_index.mmi {input.basecall_dir}/*.fastq > {params.dir}/alignment.sam 
        samtools sort --reference {input.reference} -o {output.alignment} {params.dir}/alignment.sam
        """

# Step 3: Quality assesment with Nanoplot
rule quality_check:
    """Create NanoPlot plots before and after the training. These plots show the average read length and quality of the reads."""
    input:
        basecall_dir = "basecalled_reads" + "/{version}",  # basecall summary file
        alignment = rules.align_with_minimap2.output.alignment,  # bam alignment file
        #nanoplot = config["nanoplot"]   
    params:
        version = '{version}'
    output:
        fastq = directory("nanoplot_{version}/basecalls"), # fastq plots output dir
        alignment = directory("nanoplot_{version}/alignment")  # bam alignment plots output dir
    message:
        "Checking the quality of the {input.basecall_dir} reads and the {input.alignment} with nanoplot. output to: {output}"
    shell:
        """
        NanoPlot --summary {input.basecall_dir}/sequencing_summary.txt --loglength --raw --N50 -o {output.fastq}
        NanoPlot --bam {input.alignment} --loglength --raw --N50 -o {output.alignment}
        """
        #/lib/taiyaki/venv/bin/NanoPlot --summary basecalls_test/sequencing_summary.txt --loglength -o results/Nanoplot_output_testdata
        # /lib/taiyaki/venv/bin/NanoPlot --bam evaluate/basecalls.bam --loglength -o evaluate/nanoplot_output_alignment

