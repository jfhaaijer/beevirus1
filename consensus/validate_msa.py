#!/usr/bin/env python3

from typing import Any
import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time

input_file = "/students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence_with_tool/0_iteration/msa_output.fa"
output_dir = "/students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence_with_tool/0_iteration/"


class MSA_validator:

    def __init__(self):

        args = argparser()
        self.input_file = args.inputfile
        self.output_dir = args.outputdir
        self.quickway = args.quickway
        self.quickway_limit = int(args.quickway_limit)

        # temporary dictionary for frequence of gaps per position
        self.res = {}

        # all accession names
        self.names = []

        # dictionary with frequency for gaps for every position
        self.gaps_on_pos = {}

        # amount gaps per accession
        self.sequence_validate = {}

        self.sequence_length = 0

        # score with gap penalty.
        self.name_score = {}

        # every position that contains a gap per accession
        self.name_pos = {}

        # temporary position for calculating score.
        self.temp_pos = 0
        self.csv_map = ""
        self.plot_map = ""
        self.threshold_score = {}
        self.create_directories()
        self.threshold_diff = {}

        # coverage per position variables:
        self.type = args.type
        self.cov_per_pos = {}
        self.cov = {}
        self.types = "T"
        if self.type == "rna":
            self.types = "U"

    def create_directories(self):
        """
        maakt verscihllende mapjes aan waar de csv bestanden tijdelijk in kunnen en waar de plotjes in komen te staan.
        :return:
        """
        if os.path.isdir("{}temp/".format(self.output_dir)):
            print("{}temp/ is available".format(self.output_dir))
        else:
            os.system("mkdir {}temp/".format(self.output_dir))

        if os.path.isdir("{}msa_visualisations/".format(self.output_dir)):
            print("{}msa_visualisations/ is available".format(self.output_dir))
        else:
            os.system("mkdir {}msa_visualisations/".format(self.output_dir))

        self.csv_map = "{}temp/".format(self.output_dir)
        self.plot_map = "{}msa_visualisations/".format(self.output_dir)

    def sequence_parser(self):
        """
        Opent de input file en haalt de headers eruit en maakt een list van de gebruikte sequenties aan.
        :return: een lijst van Sequenties
        """
        print("parsing of MSA file")
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                self.names.append(line)
                line = line.replace(line, "#")
                sequence += line
            else:
                sequence += line
        sequence = sequence.replace("\n", "")
        sequences = sequence.split("#")
        self.sequence_length = len(sequences[1])

        return sequences

    def check_files(self):
        """
        Kijkt of de report outfiles al bestaan. Zo ja dan worden ze verwijderd.
        :return:
        """
        if os.path.isfile("{}{}".format(self.csv_map, "Pos_gaps.csv")):
            print("removing Pos_gaps.csv file")
            os.remove("{}Pos_gaps.csv".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "Accession_gaps.csv")):
            print("removing Accession_gaps.csv file")
            os.remove("{}Accession_gaps.csv".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "gaps_align.csv")):
            print("removing gaps_align.csv file")
            os.remove("{}gaps_align.csv".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "Accession_score.csv")):
            print("removing Accession_score.csv file")
            os.remove("{}Accession_score.csv".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "Accession_threshold.csv")):
            print("removing Accession_threshold.csv file")
            os.remove("{}Accession_threshold.csv".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "coverage_per_pos_msa_gaps")):
            print("removing coverage_per_pos_msa_gaps file")
            os.remove("{}coverage_per_pos_msa_gaps".format(self.csv_map))

        if os.path.isfile("{}{}".format(self.csv_map, "coverage_per_pos_msa")):
            print("removing coverage_per_pos_msa file")
            os.remove("{}coverage_per_pos_msa".format(self.csv_map))

    def nuc_count(self, sequences):
        """
        Telt per positie het aantal gaps en de nucleotide met het hoogst procent coverage (het procent ervan wordt
        gebruikt in de berekeningen)
        :param sequences:
        :return:
        """
        print("calculating per position")

        for indx in range(len(self.names)):
            self.names[indx] = self.names[indx].split("|")[0]
            self.names[indx] = self.names[indx][1:-1]
            self.name_pos.setdefault("{}".format(self.names[indx]), [])

        direc = {}
        for i in range(len(sequences[1])):
            direc["{}".format(i)] = 0

        for i in range(len(sequences[1])):
            for a in "VHDNBXAC{}GKYSRMW".format(self.types):
                self.cov_per_pos["{}{}".format(a, i)] = 0

        print(sequences[0])
        del sequences[0]
        for index in range(len(sequences)):
            pos = 0

            self.sequence_validate["{}".format(self.names[index])] = 0

            for a in sequences[index]:
                """
                dictionary met elke posite de actg en de overige als keys en aantal als value.
                Als er characters in zitten dat dit script niet kan verwerken dan wordt dat geschreven naar de 
                info_about_consensus.txt
                """
                # cov per pos:
                if a in "VHDNBXAC{}GKYSRMW".format(self.types):
                    self.cov_per_pos['{}{}'.format(a, pos)] += 1

                elif a == "-":
                    direc["{}".format(pos)] += 1
                    self.name_pos["{}".format(self.names[index])].append(pos)
                    self.sequence_validate["{}".format(self.names[index])] += 1
                else:
                    report = "Different character spotted in the MSA, WATCH OUT! char = {}, on pos = {} in de MSA". \
                        format(a, pos)
                    print(report)

                pos += 1

        return direc

    def calculating_position_values(self, sequences, direc, pipeline):
        """
        :param sequences: lijst met sequenties
        :param direc: dictionary met getelde nucleotide op die positie.
        :param pipeline: class object voor het gebruik van classe functies
        :return:
        """

        report = "Position,coverage\n"
        pipeline.report_output_file(report, "{}coverage_per_pos_msa".format(self.csv_map))

        report = "Position,coverage\n"
        pipeline.report_output_file(report, "{}coverage_per_pos_msa_gaps".format(self.csv_map))

        for seq in sequences:
            pos = 0
            h = 0
            for a in seq:
                # maakt een tijdelijke dict aan met de count waardes per positie.
                self.res = {key: direc[key] for key in direc.keys() & {'{}'.format(pos)}}

                self.cov = {key: self.cov_per_pos[key] for key in self.cov_per_pos.keys() &
                            {'A{}'.format(pos), 'G{}'.format(pos), '{}{}'.format(self.types, pos), 'C{}'.format(pos),
                             'M{}'.format(pos), 'R{}'.format(pos), 'W{}'.format(pos), 'S{}'.format(pos),
                             'Y{}'.format(pos), 'K{}'.format(pos), 'V{}'.format(pos), 'H{}'.format(pos),
                             'N{}'.format(pos), 'X{}'.format(pos), 'B{}'.format(pos), 'D{}'.format(pos)}}
                all_values = self.cov.values()
                max_value = max(all_values)
                pipeline.coverage_per_pos_csv(max_value, pos, pipeline)
                pipeline.calculate_threshold(pos)
                pos += 1

                h = 2
            # moet gaan breken als hij alle posities heeft gehad zodat hij dit niet nog 20 keer gaat uitvoern door de
            # for loop van seq in sequences.
            if h == 2:
                break
        report = "pos,naam,yas,col"
        pipeline.report_output_file(report, "{}gaps_align.csv".format(self.csv_map))
        posi = 1

        sorting = sorted(self.sequence_validate, key=self.sequence_validate.get, reverse=True)

        for i in sorting:
            for position in self.name_pos["{}".format(i)]:
                report = "{},{},{},{}\n".format(position, i, posi, "red")
                pipeline.report_output_file(report, "{}gaps_align.csv".format(self.csv_map))

            posi += 1

        print(sorting)

    def coverage_per_pos_csv(self, max_value, pos, pipeline):
        """
        reports the coverage per positions.
        :param max_value:
        :param pos:
        :param pipeline:
        :return:
        """

        report = "{},{}\n".format(pos, max_value / sum(self.cov.values()) * 100)
        pipeline.report_output_file(report, "{}coverage_per_pos_msa".format(self.csv_map))

        report = "{},{}\n".format(pos, max_value / len(self.names) * 100)
        pipeline.report_output_file(report, "{}coverage_per_pos_msa_gaps".format(self.csv_map))

    def calculate_threshold(self, pos):
        """
        berekent de score
        :param pos:
        :return:
        """
        self.gaps_on_pos["{}".format(pos)] = sum(self.res.values())

    def make_score(self):
        """
        berekent de score per sequentie
        :return:
        """

        for i in self.names:
            self.name_score["{}".format(i)] = 0
            self.threshold_score["{}".format(i)] = 0
            self.threshold_diff["{}".format(i)] = 0

        for i in self.names:
            for item in self.name_pos["{}".format(i)]:
                if item <= 0.05 * self.sequence_length:
                    self.sequence_validate["{}".format(i)] -= 1
                else:
                    if item == 1:
                        self.name_score["{}".format(i)] = 10
                    elif item - self.temp_pos == 1:
                        self.name_score["{}".format(i)] += 1
                    else:
                        self.name_score["{}".format(i)] += 10
                self.temp_pos = item
        print(self.name_score)
        return 0

    def threshold_difference(self, pipe_line, threshold, score, accession, color):
        """
        berekent het score verschil tegenover de threshold van die sequentie
        :param pipe_line:
        :param threshold:
        :param score:
        :param accession:
        :param color:
        :return:
        """
        self.threshold_score["{}".format(accession)] = score - threshold
        report = "{},{},{},{},{}\n".format(accession, round(score - threshold, 2), score, threshold, color)
        pipe_line.report_output_file(report, "{}Accession_threshold.csv".format(self.csv_map))
        self.threshold_diff["{}".format(accession)] = round(score - threshold, 2)
        return 0

    def threshold_crossing(self, pipe_line):
        """
        Kijkt of sequentie hoger scoren dan hun thresholds zo ja dan wordt de slechtste scorende in bad_accession.txt
        gezet.
        :param pipe_line:
        :return:
        """

        sorting3 = sorted(self.threshold_diff, key=self.threshold_diff.get, reverse=True)
        os.system("cp bad_accession.txt {}".format(self.output_dir))

        if self.quickway == "f":
            if self.threshold_diff["{}".format(sorting3[0])] > 0:
                report = "{}\n".format(sorting3[0])
                pipe_line.report_output_file(report, "bad_accession.txt")
        else:
            limit = 0
            for i in sorting3:
                if self.threshold_diff["{}".format(i)] > 0 and limit != self.quickway_limit:
                    report = "{}\n".format(i)
                    pipe_line.report_output_file(report, "bad_accession.txt")
                limit += 1
        return 0

    def position_gaps_csv(self, pipe_line):
        """
        zet de gaps per positie in een csv file
        :param pipe_line:
        :return:
        """
        report = "Position,Gaps,Gaps_encountered,color\n"
        pipe_line.report_output_file(report, "{}Pos_gaps.csv".format(self.csv_map))
        report = ""
        for num in range(self.sequence_length):
            report += "{},".format(num)
            report += "{},".format(self.gaps_on_pos["{}".format(num)])
            if self.gaps_on_pos["{}".format(num)] is not 0:
                report += "{},".format(1)
                report += "red"
            else:
                report += "{},".format(0)
                report += "green"
            report += "\n"

            pipe_line.report_output_file(report, "{}Pos_gaps.csv".format(self.csv_map))
            report = ""

    def accession_gaps_csv(self, pipe_line):
        """
        zet de gaps per sequentie in een csv file.
        :param pipe_line:
        :return:
        """
        report = "accession,Gaps\n"
        pipe_line.report_output_file(report, "{}Accession_gaps.csv".format(self.csv_map))
        report = ""
        for i in self.sequence_validate:
            report += "{},".format(i)
            report += "{}".format(self.sequence_validate["{}".format(i)])

            report += "\n"
            pipe_line.report_output_file(report, "{}Accession_gaps.csv".format(self.csv_map))
            report = ""

    def accession_score_csv(self, pipe_line):
        """
        zet de score per accessie code in een csv file
        :param pipe_line:
        :return:
        """
        report = "accession,Gaps,color,threshold\n"
        pipe_line.report_output_file(report, "{}Accession_score.csv".format(self.csv_map))

        report = "accession,difference,score,threshold,color\n"
        pipe_line.report_output_file(report, "{}Accession_threshold.csv".format(self.csv_map))

        report = ""
        sorting2 = sorted(self.name_score, key=self.name_score.get, reverse=True)

        for i in sorting2:
            report += "{},".format(i)
            report += "{},".format(self.name_score["{}".format(i)] * 0.1)

            threshold = (self.sequence_validate["{}".format(i)] * 10 * 0.1 + 0.9 * (
                self.sequence_validate["{}".format(i)])) * 0.1
            threshold = threshold + round(
                ((self.sequence_length * 0.002) * 10 * 0.1 + (self.sequence_length * 0.002) * 0.9) * 0.1)

            if (self.name_score["{}".format(i)] * 0.1) < threshold:
                report += "green"
                col = "green"
            else:
                report += "red"
                col = "red"

            pipe_line.threshold_difference(pipe_line, threshold, self.name_score["{}".format(i)] * 0.1, i, col)

            report += ",{}".format(threshold)
            report += "\n"
            pipe_line.report_output_file(report, "{}Accession_score.csv".format(self.csv_map))
            report = ""

    def gaps_per_accession_plot(self):
        print("making first plot")
        os.system(
            "Rscript gaps_per_accession.R {}Accession_gaps.csv {}gaps_per_accession.jpeg {} ".format(
                self.csv_map,
                self.plot_map,
                self.sequence_length))
        return 0

    def gaps_per_position_plot(self):
        print("making second plot")
        os.system(
            "Rscript gaps_per_position.R {}Pos_gaps.csv {}gaps_per_position.jpeg".format(
                self.csv_map,
                self.plot_map))
        return 0

    def frequency_gaps_per_position_plot(self):
        print("making third plot")
        os.system(
            "Rscript frequence_gaps_pos.R {}Pos_gaps.csv {}frequency_gaps_per_position.jpeg".format(
                self.csv_map,
                self.plot_map))
        return 0

    def aligned_gaps_plot(self):
        print("making fourth plot")
        os.system(
            "Rscript gaps_align.R {}gaps_align.csv {}aligned_gaps.jpeg {} ".format(
                self.csv_map,
                self.plot_map,
                self.sequence_length))
        return 0

    def accession_score_plot(self):
        print("making fifth plot")
        os.system(
            "Rscript accession_score.R {}Accession_score.csv {}Accession_score.jpeg {} ".format(
                self.csv_map,
                self.plot_map,
                self.sequence_length))
        return 0

    def make_threshold_differenc_plot(self):
        print("making sixth plot")
        os.system(
            "Rscript threshold_difference.R {}Accession_threshold.csv {}Accession_threshold_score.jpeg".format(
                self.csv_map,
                self.plot_map, ))

        return 0

    def coverage_per_pos_plot_gaps(self):
        print("making seventh plot")
        os.system(
            "Rscript coverage_per_pos_plot_gaps.R "
            "{}coverage_per_pos_msa_gaps {}coverage_per_pos_msa_gaps.jpeg".format(
                self.csv_map,
                self.plot_map, ))

    def coverage_per_pos_plot_no_gaps(self):
        print("making eighth plot")
        os.system(
            "Rscript coverage_per_pos_plot_no_gaps.R "
            "{}coverage_per_pos_msa {}coverage_per_pos_msa.jpeg".format(
                self.csv_map,
                self.plot_map, ))

    def report_output_file(self, report, directory):
        """
        :param directory: complete directory for writing the file.
        :param report: Is de zin die aan de file wordt toegevoegd
        :param name: Zegt in welke file hij moet schrijven.
        :return:
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}".format(directory)):
            with open("{}".format(directory), 'a') as file:
                file.write('{}'.format(report))
                file.close()
        else:
            with open("{}".format(directory), "w+") as file:
                file.write(report)
                file.close()

    def remove_temp(self):
        print("removing temp directory")
        os.system("rm -rf {}temp/".format(self.output_dir))


def argparser():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert the filename of the input file must be a msa output .fasta file.")
    parser.add_argument("--outputdir", default=output_dir,
                        help="insert the output directory.")
    parser.add_argument("--quickway", default="f",
                        help="t for every accession remove max 5 above threshold per iteration, f for only one per "
                             "iteration.")
    parser.add_argument("--type", default="dna",
                        help="was the msa of dna or rna?")

    parser.add_argument("--quickway_limit", default=5,
                        help="set the max limit every iteration removes (when quickway=t)")

    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren
    start = time.time()

    pipe_line = MSA_validator()
    pipe_line.check_files()
    seq = pipe_line.sequence_parser()
    direc = pipe_line.nuc_count(seq)
    pipe_line.calculating_position_values(seq, direc, pipe_line)

    print("writing pos_gaps csv_file")
    pipe_line.position_gaps_csv(pipe_line)

    print("writing accession_gaps csv_file")
    pipe_line.accession_gaps_csv(pipe_line)

    pipe_line.make_score()
    print("writing accession_score csv_file")
    pipe_line.accession_score_csv(pipe_line)

    pipe_line.threshold_crossing(pipe_line)

    pipe_line.gaps_per_accession_plot()
    pipe_line.gaps_per_position_plot()
    pipe_line.frequency_gaps_per_position_plot()
    pipe_line.aligned_gaps_plot()
    pipe_line.accession_score_plot()
    pipe_line.make_threshold_differenc_plot()
    pipe_line.coverage_per_pos_plot_no_gaps()
    pipe_line.coverage_per_pos_plot_gaps()

    pipe_line.remove_temp()

    end = time.time()
    react_time = end - start
    print("It took the program {} minutes to complete.".format(round(round(react_time) / 60)))

    return 0


if __name__ == '__main__':
    sys.exit(main())
