args = commandArgs(trailingOnly=TRUE)
data <- read.csv(args[1] , header = T, sep = ",")
jpeg(args[2])

data <- data[order(-data$difference),]
xas <- c(1:length(data$accession))
plot(xas,y=as.integer(data$difference),main="Score difference with threshold per sequence", col=as.character(data$color), type='h', lwd=5, 
     ylim=c(min(data$difference)-10, max(data$difference)+10), xaxt="none", ylab="", xlab="")
rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col =  rgb(red = 0, green = 1, blue = 1, alpha = 0.1))
legend("topright", inset=.02, legend=c("Above threshold", "Under threshold"),col=c("red", "green"),  fill=c("red", "green"), cex=0.8,bg=rgb(red = 0, green = 1, blue = 1, alpha = 0.1))

axis(side=1,at=xas,labels=data$accession, las=2, cex.axis=0.8)

points(xas, data$difference, pch=1, lwd=10, col=as.character(data$color))
lines(xas,rep(0,times=length(data$accession)), lwd=2)
text(xas+0.2, data$difference+1, data$difference,cex=0.7,pos=3,srt=45)
mtext(text = "Accession numbers",
      side = 1,line = 4)

# y axis
mtext(text = "Score difference with threshold",
      side = 2, #side 2 = left
      line = 3, cex=1)

dev.off()