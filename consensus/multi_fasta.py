#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time

"""
Convert a DNA mutli fasta to RNA multi_fasta, removes bad accession if there are any. And could combine two multi
fasta files
"""

input_file = "/students/2019-2020//Thema11/minion_js_jh/msa/data/all_world.fasta"
output_file = "/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/world_default/world_default.fasta"

"""
opens the bad_accession.txt and looks for accession code to be removed from the multi-fasta file(s)
"""


class Multi_fasta_processor:
    """
    The multi_fasta_processor class Will preprocesses with the help of some parameters
    so it can later be used for the MSA
    """

    def __init__(self):
        """
        Takes input file, output file, accession codes and types (optionally a second input file)
        """

        args = argparser()

        self.input_file = args.inputfile
        self.output_file = args.outputfile
        self.accession = self.bad_accession_opener()

        self.secondinput = args.secondinputfile
        self.types = args.types

        if type(self.accession) == list:
            print('true')
        else:
            self.accession = [self.accession]

    def bad_accession_opener(self):
        """
        checkt de bad_accession.txt op slechte accessie codes om die uit de multi fasta file te halen.
        :return:
        """
        op_file = open("bad_accession.txt")
        accession = []
        accession_together = ""
        for line in op_file:
            line = line.strip()
            accession.append(line)
            accession_together += "{} \n".format(line)

        print("{} names are being removed".format(accession_together))
        return accession

    def sequence_parser(self):
        """
        Open input file(s) and makes everything RNA or DNA, also it shortens certain accession codes.
        It also functions as a poly-a Tail remover.

        returns preprocessed sequences
        """
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                line = line.replace("Deformed wing virus ", "DWV ")
                line = line.replace("| complete genome", "")
                print(line)
                line = line.replace(">", "#>")
                sequence += line
            else:
                if self.types == "dna":
                    line = line.replace("U", "T")
                    sequence += line
                elif self.types == "rna":
                    line = line.replace("T", "U")
                    sequence += line
                else:
                    print("choose as parameters in --type=rna or --type=dna")

        # als er een tweede multi_fasta file is en ook daadwerkelijk bestaat.
        if self.secondinput != "none" and os.path.isfile(self.secondinput):
            op_file = open("{}".format(self.secondinput))
            for line in op_file:
                if line.startswith(">"):
                    line = line.replace("Deformed wing virus ", "DWV ")
                    line = line.replace("| complete genome", "")
                    print(line)
                    line = line.replace(">", "#>")
                    sequence += line
                else:
                    if self.types == "dna":
                        line = line.replace("U", "T")
                        sequence += line
                    elif self.types == "rna":
                        line = line.replace("T", "U")
                        sequence += line
                    else:
                        print("choose as parameters in --type=rna or --type=dna")

        sequences = sequence.split("#")

        indx = 0
        for seq in sequences:
            times = 0
            while sequences[indx].endswith("A") or sequences[indx].endswith("\n"):
                times += 1
                sequences[indx] = sequences[indx][:-1]

            sequences[indx] += "\n"
            indx += 1

        return sequences

    def check_files(self):
        """
        Looks if output files are already there. if so the file will be removed.
        :return:
        """
        if os.path.isfile("{}".format(self.output_file)):
            print("{}".format(self.output_file))
            os.remove("{}".format(self.output_file))

    def accession_remover(self, sequences, pipe_line):
        """
        :param sequences: List with every sequence.
        :param pipe_line: object for executing class function.
        :return: The sequences of every (good) accession code.
        """
        index = 0
        fasta = ""

        for access in self.accession:
            if not access == "":
                for seq in sequences:
                    if seq.startswith(">" + access):
                        print("removing sequences with this accession.")
                        print(access)
                        del sequences[index]
                    index += 1
                index = 0

        for seq in sequences:
            fasta += seq

        pipe_line.report_output_file(fasta)
        return sequences

    def report_output_file(self, report):
        """
        If file does not exist it will be made, if it does exist the report will be added to the file.
        :param report: Is the String that will be added to the file.
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}".format(self.output_file)):
            print("print this shouldnt happen")
            with open("{}".format(self.output_file), 'a') as file:
                file.write('{}'.format(report))
                file.close()
        else:
            print("making file")
            with open("{}".format(self.output_file), "w+") as file:
                file.write(report)
                file.close()


def argparser():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert a multifasta file Example:"
                             "data/all_world.fasta")
    parser.add_argument("--outputfile", default=output_file,
                        help="insert the filename of the output file. It is a fasta so must end with .fa or .fasta"
                             "Example: /output/multi_fasta.fasta")

    parser.add_argument("--secondinputfile", default="none",
                        help="if you want to proccess two different multi fasta files")

    parser.add_argument("--types", default="dna",
                        help="You can choose if you want the msa in rna or dna.")

    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren

    pipe_line = Multi_fasta_processor()
    pipe_line.check_files()
    seq = pipe_line.sequence_parser()
    pipe_line.accession_remover(seq, pipe_line)
    return 0


if __name__ == '__main__':
    sys.exit(main())
