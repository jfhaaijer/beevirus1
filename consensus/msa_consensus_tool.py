#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time
from os import listdir

input_file = "/students/2019-2020//Thema11/minion_js_jh/msa/data/all_world.fasta"
output_dir = "/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/test_tool/"


class MSA_consensus_tool:

    def __init__(self):

        args = argparser()

        self.input_file = args.inputfile
        self.output_file = args.outputdir
        self.quickway = args.quickway
        self.iteration = args.iteration
        self.maxiteration = args.maxiteration
        self.continue_last_session = args.last_session

        self.iterations = self.count_directories()
        self.output_dir = ""

        self.before_length_bad = 0
        self.after_length_bad = 0

        self.secondinput = args.secondinputfile
        self.types = args.types

        self.iteration_max = 0

        self.bad_acces()

    def bad_acces(self):
        """
        bekijkt of bad_accession.txt aanwezig is en of de parameter van continue_last_session op yes is gezet.
        :return:
        """
        if os.path.isfile("bad_accession.txt"):
            if self.continue_last_session == "no":
                os.system("rm bad_accession.txt")
            elif self.continue_last_session == "yes":
                print("using previous bad_accession.txt")

            else:
                print("something else than the values acceptable was used. Exiting program.")
                print("You typed: {}. while it can only be yes or no. Look at caps locks.".format(self.continue_last_session))
                sys.exit()
            return iter

    def executing_executables(self, pipe_line):
        """
        Als de iteratie parameter op t is gezet dan voert hij de while loop uit.
        :param pipe_line:
        :return:
        """
        if self.iteration == "t":
            pipe_line.while_loop(pipe_line)
            print(self.iterations)
            print(self.maxiteration)
        else:
            pipe_line.bad_accession_file()
            pipe_line.make_outputfile()
            pipe_line.executables()
            #os.system("cp {}0_iteration/msa_output.fa".format(output_file,self.iteration))
        return

    def count_directories(self):
        """
        telt het aantal directories dat eindigen met _iteration om de volgende iteratie niet in een bestaande file
        gaat beginnnen
        :return:
        """
        a = 0
        for i in listdir(self.output_file):
            if i.endswith("_iteration"):
                a += 1

        return a


    def bad_accession_file(self):
        """
        kijkt in de bad_accession.txt of er accesie codes staan die eruit gehaald moeten worden.
        :return: 
        """
        length_bad_access = 0
        if not os.path.isfile("bad_accession.txt"):
            with open("bad_accession.txt", "w+") as file:
                file.write("")
                file.close()
            op_file = open("{}".format("bad_accession.txt"))
            for line in op_file:
                length_bad_access += 1
        else:
            op_file = open("{}".format("bad_accession.txt"))
            for line in op_file:
                length_bad_access += 1

        return length_bad_access

    def make_outputfile(self):
        """
        maakt voor elke iteratie een apart mapje.
        :return:
        """
        if os.path.isdir("{}".format(self.output_file)):
            if os.path.isdir("{}{}_iteration/".format(self.output_file, str(self.iterations))):
                os.system("rm -rf {}{}_iteration/".format(self.output_file, str(self.iterations)))
            os.system("mkdir {}{}_iteration/".format(self.output_file, str(self.iterations)))
            self.output_dir = "{}{}_iteration/".format(self.output_file, str(self.iterations))
        else:
            print("make a directory for the output.")

    def executables(self):
        """
        voert de verschillende files 1 keer uit.
        :return:
        """
        os.system("python multi_fasta.py --inputfile={} --outputfile={}multi_fasta.fasta --secondinputfile={} --type={}".format(self.input_file, self.output_dir, self.secondinput, self.types))

        seq = ""
        op_file = open("{}multi_fasta.fasta".format(self.output_dir))
        for line in op_file:
            seq+=line

        limit = len(seq.split(">"))

        os.system("clustalo -i {}multi_fasta.fasta -o {}msa_output.fa --threads=60 -v".format(self.output_dir, self.output_dir))

        os.system("rm {}multi_fasta.fasta".format(self.output_dir))

        if not limit > 500:
            os.system("python validate_msa.py --inputfile={}msa_output.fa --outputfile={} --quickway={} --type={}".format(self.output_dir, self.output_dir, self.quickway, self.types))
        else:
            print("Have some patient. It will probably take more than an hour to compute. So give yourself a break and do something fun.")
            os.system("python validate_msa.py --inputfile={}msa_output.fa --outputfile={} --quickway={} --type={}".format(
                    self.output_dir, self.output_dir, self.quickway, self.types))
        return 0

    def while_loop(self, pipe_line):
        """
        voert de verschillende scripts uit in een iteratieve manier.
        :param pipe_line:
        :return:
        """

        print("iterations {}".format(self.iterations))

        pipe_line.make_outputfile()

        self.before_length_bad = pipe_line.bad_accession_file()

        pipe_line.executables()

        self.iterations += 1
        self.iteration_max += 1

        self.after_length_bad = pipe_line.bad_accession_file()

        while self.before_length_bad != self.after_length_bad and self.iteration_max != int("{}".format(self.maxiteration)):

            self.before_length_bad = pipe_line.bad_accession_file()

            pipe_line.make_outputfile()
            pipe_line.executables()

            self.iterations += 1
            self.iteration_max += 1
            self.after_length_bad = pipe_line.bad_accession_file()

    def sequence_parser(self):
        """
        Opent de input file en haalt de headers eruit en maakt een list van de gebruikte sequenties aan.
        :return: een lijst van Sequenties
        """
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                print(line)
            else:
                sequence += line

        return sequences

    def check_files(self):
        """
        Kijkt of de report outfiles al bestaan. Zo ja dan worden ze verwijderd.
        :return:
        """
        if os.path.isfile("{}".format(self.output_file)):
            print("{}".format(self.output_file))
            os.remove("{}".format(self.output_file))


    def report_output_file(self, report):
        """

        :param report: Is de zin die aan de file wordt toegevoegd
        :param name: Zegt in welke file hij moet schrijven.
        :return:
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}".format(self.output_file)):
            print("print this shouldnt happen")
            with open("{}".format(self.output_file), 'a') as file:
                file.write('{}'.format(report))
                file.close()
        else:
            print("making file")
            with open("{}".format(self.output_file), "w+") as file:
                file.write(report)
                file.close()

def argparser():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert a multifasta file Example:"
                             "/data/all_world.fasta")
    parser.add_argument("--outputdir", default=output_dir,
                        help="must be a directory")
    parser.add_argument("--quickway", default="f",
                        help="Want to delete 5 or more accession above the threshold at once? set --quickway=t")
    parser.add_argument("--iteration", default="f",
                        help="If you want to make use of the iteration function type --iteration=t")
    parser.add_argument("--maxiteration", default=1,
                        help="type 'none' for unlimited iterations until no accession above threshold,"
                             " default is set to 1. ")

    parser.add_argument("--secondinputfile", default="none",
                        help="if you want to do a msa of two different multifasta files combined.")

    parser.add_argument("--types", default="dna",
                        help="You can choose if you want the msa in rna or dna.")

    parser.add_argument("--last_session", default="no",
                        help="If you want to continue last session set --last_session=yes")

    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren

    pipe_line = MSA_consensus_tool()
    pipe_line.bad_accession_file()
    pipe_line.executing_executables(pipe_line)

    return 0


if __name__ == '__main__':
    sys.exit(main())
