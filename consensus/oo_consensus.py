#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import operator
import random
from itertools import chain
import os.path
import time
from decimal import Decimal

"""
Berekent een consensus sequentie door eerst een dictionary te maken met elk van de letters met de bij behorende positie 
als key met als value het aantal keer dat die letter op die positie voorkomt. Dan kun je de nucleotide gaan tellen per 
sequentie op die plek. Als er bijvoorbeeld twee nucleotide even vaak voorkomen dan wordt er eerst gekeken of de 
ambiguity letters maar een van de twee letters kan zijn zo ja dan wordt de letter met wie hij kan matchen gebruikt als 
nucleotide voor die plek, zo niet dan wordt er random gekozen tussen die twee nucleotiden. Als er meer gaps in voorkomen
dan letters of de getelde letter een coverage van 1 hebben op die positie dan wordt er niks van die positie van de msa 
in de consensus sequentie gedaan. De informatie over de consensus komen in een apart mapje terecht.
"""

input_file = "/students/2019-2020/Thema11/minion_js_jh/msa/jimjim_test/0_iteration/msa_output.fa"
output_file = "/students/2019-2020/Thema11/minion_js_jh/msa/jimjim_test/0_iteration/jimjimtest_consensus.fasta"
report_dir = "/students/2019-2020/Thema11/minion_js_jh/msa/jimjim_test/0_iteration/"


class Consensus:

    def __init__(self):
        """
        Krijgt een input_file naam, output_file naam, perfectlength, count, report_dir naam mee.
        Als de meegegeven counts percentage niet hoger is dan 50% dan wordt de default waarde van counts gepakt.
        """

        args = argparser()
        if int(args.counts) > 50:
            args.counts = args.counts
        else:
            print("counts argument must be higher than 50%. default 100% is now used")
            args.counts = 100
        self.input_file = args.inputfile
        self.output_file = args.outputfile
        self.perfectlength = args.perfectlength
        self.counts = args.counts
        self.report_file = args.reportdir
        # DNA or RNA
        self.type = args.type

        # creates a map for all the output for the consensus.
        if not os.path.isdir("{}consensus_evaluation/".format(self.report_file)):
            os.system("mkdir {}consensus_evaluation/".format(self.report_file))
        self.output_dir = self.report_file + "consensus_evaluation/"

        self.types = "T"
        if self.type == "rna":
            self.types = "U"

        # consensus houdt de letters bij die in de consensus gebruikt worden.
        self.consensus = ""
        # perf_count wordt gebruikt om alle nucleotiden te tellen die bij alle sequenties in de msa op een plek
        # hetzelfde zijn
        self.perf_count = 0
        # Houdt bij hoevaak er niks ipv een letter in de sequentie wordt geplaats. Wordt gebruikt om de juiste posities
        # in de consensus bij te houden.
        self.corrected_pos = 0
        # Wordt als de vorige positie waarbij alle sequenties de dezelfde nucleotide op 1 plek hadden.
        self.post = 0
        # Houdt bij hoeveel van de nucleotide  waarbij alle sequenties de dezelfde nucleotide op 1 plek hadden op een
        # rij zijn.
        self.perfect_called = 0
        # Houdt de eerst positie bij van de sequentie waarbij de nucleotide achter elkaar waarbij alle sequenties de
        # dezelfde nucleotide op 1 plek hadden bij te houden.
        self.first_pos = 0
        # Houdt sequentie bij van de nucleotide waarbij alle sequenties de dezelfde nucleotide op 1 plek hadden op een
        # rij zijn van minsten 5 groot zijn..
        self.perfect_seq = ""

        # header for consensus sequence.
        self.header = args.header

        self.post_mut = 0
        self.mut_count = 0
        self.mutant_called = 0
        self.mutant_seq = ""
        self.first_pos_mut = 0

        self.letter = ""

        # kijk naar hoe vaak de verschillende coverage procenten voorkomen in de consensus sequentie
        self.zerototen = 0
        self.tentotwenty = 0
        self.twentytothirty = 0
        self.thirtytofourthy = 0
        self.fourthytofifty = 0
        self.fiftytosixty = 0
        self.sixtytoseventy = 0
        self.seventytoeighty = 0
        self.eightytoninety = 0
        self.ninetytohundred = 0

        self.zerototen1 = 0
        self.tentotwenty1 = 0
        self.twentytothirty1 = 0
        self.thirtytofourthy1 = 0
        self.fourthytofifty1 = 0
        self.fiftytosixty1 = 0
        self.sixtytoseventy1 = 0
        self.seventytoeighty1 = 0
        self.eightytoninety1 = 0
        self.ninetytohundred1 = 0

        self.res = {}

        self.counted_nuc_on_pos = 1

        # threshold for maximum percentage
        self.threshold = int(Decimal(args.maximum_gaps))

        self.mutation_pos_consensus = []
        self.mutation_pos_msa = []

        self.mutatie_regios = 0

        self.sequence_length = 0

        self.amount_seq = 0

    def sequence_parser(self):
        """
        Opent de input file en haalt de headers eruit en maakt een list van de gebruikte sequenties aan.
        :return: een lijst van Sequenties
        """
        sequence = ''
        op_file = open("{}".format(self.input_file))
        for line in op_file:
            if line.startswith(">"):
                line = line.replace(line, "#")
                sequence += line
            else:
                sequence += line
        sequence = sequence.replace("\n", "")
        sequences = sequence.split("#")
        return sequences

    def check_files(self):
        """
        Kijkt of de report outfiles al bestaan. Zo ja dan worden ze verwijderd.
        :return:
        """
        if os.path.isfile("{}{}".format(self.output_dir, "correct_sequences.txt")):
            print("removing correct_sequences file")
            os.remove("{}correct_sequences.txt".format(self.output_dir))
        if os.path.isfile("{}{}".format(self.output_dir, "Nuc_changes.txt")):
            print("removing Nuc_changes file")
            os.remove("{}Nuc_changes.txt".format(self.output_dir))
        if os.path.isfile("{}{}".format(self.output_dir, "Info_about_consensus.txt")):
            print("removing Info_about_consensus file")
            os.remove("{}Info_about_consensus.txt".format(self.output_dir))
        if os.path.isfile("{}{}".format(self.output_dir, "mutant_sequences.txt")):
            print("removing  mutant_sequences.txt")
            os.remove("{}mutant_sequences.txt".format(self.output_dir))
        if os.path.isfile("{}{}".format(self.output_dir, "mutations")):
            print("removing  mutations")
            os.remove("{}mutations".format(self.output_dir))

        if os.path.isfile("{}{}".format(self.output_dir, "coverage_per_pos_consensus")):
            print("removing  coverage_per_pos_consensus")
            os.remove("{}coverage_per_pos_consensus".format(self.output_dir))
        if os.path.isfile("{}{}".format(self.output_dir, "coverage_per_pos_consensus_gaps")):
            print("removing  coverage_per_pos_consensus_gaps")
            os.remove("{}coverage_per_pos_consensus_gaps".format(self.output_dir))

        if os.path.isfile("{}".format(self.output_file)):
            print("removing  {}".format(self.output_file))
            os.remove("{}".format(self.output_file))

    def nuc_count(self, sequences, pipeline):
        """
        :param sequences: Lijst met elke sequentie.
        :param pipeline: object
        :return: een dictionary met de getelde nucleotide op elke positie.
        """

        direc = {}

        report = "Position_msa,Position_consensus,mutations,mutation_region,color\n"
        pipeline.report_output_file(report, "mutations")
        self.sequence_length = len(sequences[1])

        for seq in sequences:
            pos = 0

            self.amount_seq += 1
            for a in seq:
                """
                dictionary met elke posite de actg en de overige als keys en aantal als value.
                Als er characters in zitten dat dit script niet kan verwerken dan wordt dat geschreven naar de 
                info_about_consensus.txt
                """

                if a in "VHDNBXAC{}G-KYSRMW".format(self.types):
                    direc['{}{}'.format(a, pos)] = direc.setdefault('{}{}'.format(a, pos), 0) + 1
                else:
                    report = "Different character spotted in the MSA, WATCH OUT! char = {}, on pos = {} in de MSA". \
                        format(a, pos)
                    print(report)
                    pipeline.report_output_file(report, "Info_about_consensus.txt")
                pos += 1

        return direc

    def highest_gaps(self, pos):
        """
        haalt the gap weg uit the tijdelijke dictionary
        :param pos: positie op het MSA
        :return:
        """
        del self.res["-{}".format(pos)]

    def write_consensus(self, sequences, direc, pipeline):
        """
        :param sequences: lijst met sequenties
        :param direc: dictionary met getelde nucleotide op die positie.
        :param pipeline: class object voor het gebruik van classe functies
        :return:
        """
        header = ">{}\n".format(self.header)

        # aantal sequenties gebruikt bij de MSA
        percentage = len(sequences) - 1

        report = "Position,coverage\n"
        pipeline.report_output_file(report, "coverage_per_pos_consensus")

        report = "Position,coverage\n"
        pipeline.report_output_file(report, "coverage_per_pos_consensus_gaps")

        for seq in sequences:
            pos = 0
            h = 0
            for a in seq:
                # maakt een tijdelijke dict aan met de count waardes per positie.
                self.res = {key: direc[key] for key in direc.keys() &
                            {'A{}'.format(pos), 'G{}'.format(pos), '{}{}'.format(self.types,pos), 'C{}'.format(pos),
                             'M{}'.format(pos), 'R{}'.format(pos), 'W{}'.format(pos), 'S{}'.format(pos),
                             'Y{}'.format(pos), 'K{}'.format(pos), 'V{}'.format(pos), 'H{}'.format(pos),
                             'N{}'.format(pos), 'X{}'.format(pos), 'B{}'.format(pos), 'D{}'.format(pos),
                             '-{}'.format(pos)}}

                pipeline.calculating_consensus(pipeline, pos, percentage)

                pos += 1

                h = 2
            # moet gaan breken als hij alle posities heeft gehad zodat hij dit niet nog 20 keer gaat uitvoern door de
            # for loop van seq in sequences.
            if h == 2:
                break
        # zet om de 70 letter een enter neer.
        consensust = re.sub("(.{70})", "\\1\n", self.consensus, 0, re.DOTALL)
        complete_consensus = header + consensust
        print(complete_consensus)

        # als de outputfile nog niet bestaat maak hem aan.
        if os.path.isfile("{}".format(self.output_file)):
            with open("{}".format(self.output_file), 'a') as file:
                file.write(report)
                file.close()
        else:
            with open("{}".format(self.output_file), "w+") as file:
                file.write(report)
                file.close()

        coverage = (self.fiftytosixty + self.sixtytoseventy + self.seventytoeighty +
                    self.eightytoninety + self.ninetytohundred) / len(self.consensus) * 100

        coverage2 = (self.fiftytosixty1 + self.sixtytoseventy1 + self.seventytoeighty1 +
                     self.eightytoninety1 + self.ninetytohundred1) / len(self.consensus) * 100

        print(self.zerototen1 + self.tentotwenty1 + self.twentytothirty1 + self.thirtytofourthy1 + self.fourthytofifty1
              + self.fiftytosixty1 + self.sixtytoseventy1 + self.seventytoeighty1 + self.eightytoninety1 +
              self.ninetytohundred1)

        report = "MSA is {} long\n There are {} nucleotides that all the sequences in the MSA agree on.\n" \
                 "There were {} cases in which there was: only gaps/only gaps or only ambiguous letters and gaps/or " \
                 "not " \
                 "enough coverage for the position and wil not be placed in the consensus sequence\nThe length of " \
                 "the " \
                 "consensus sequence is {}\n\nNumber of letter for the consensus sequence with different percentage " \
                 "coverage. Must have a coverage of atleas 2 nucleotides:\n" \
                 "(If there are zero counts in the 0% until 10% or until 20% it could be because 10% or 20% " \
                 "of the amount of sequences used isn't more than 1 and only nucs with a coverage more than 50% of " \
                 "all" \
                 " the sequences in the msa are being used for the consensus.)\n" \
                 "coverage between 0% and 10%: {}\n" \
                 "coverage between 10% and 20%: {}\n" \
                 "coverage between 20% and 30%: {}\n" \
                 "coverage between 30% and 40%: {}\n" \
                 "coverage between 40% and 50%: {}\n" \
                 "coverage between 50% and 60%: {}\n" \
                 "coverage between 60% and 70%: {}\n" \
                 "coverage between 70% and 80%: {}\n" \
                 "coverage between 80% and 90%: {}\n" \
                 "coverage between 90% and 100%: {}\n" \
                 "\n" \
                 "percentage of the consensus sequence that has a coverage of more than 50%: {}%" \
                 "\n" \
                 "\n" \
                 "of the amount of sequences used isn't more than 1 and only nucs with a coverage more than 50% of " \
                 "all" \
                 " the sequences in the msa are being used for the consensus.)\n" \
                 "coverage between 0% and 10%: {}\n" \
                 "coverage between 10% and 20%: {}\n" \
                 "coverage between 20% and 30%: {}\n" \
                 "coverage between 30% and 40%: {}\n" \
                 "coverage between 40% and 50%: {}\n" \
                 "coverage between 50% and 60%: {}\n" \
                 "coverage between 60% and 70%: {}\n" \
                 "coverage between 70% and 80%: {}\n" \
                 "coverage between 80% and 90%: {}\n" \
                 "coverage between 90% and 100%: {}\n" \
                 "\n" \
                 "percentage of the consensus sequence that has a coverage of more than 50%: {}%" \
                 "\namount of sequences used: {}\n".format(pos, self.perf_count, self.corrected_pos, len(self.consensus), self.zerototen,
                           self.tentotwenty, self.twentytothirty,
                           self.thirtytofourthy, self.fourthytofifty, self.fiftytosixty, self.sixtytoseventy,
                           self.seventytoeighty,
                           self.eightytoninety, self.ninetytohundred, round(coverage, 2), self.zerototen1,
                           self.tentotwenty1, self.twentytothirty1,
                           self.thirtytofourthy1, self.fourthytofifty1, self.fiftytosixty1, self.sixtytoseventy1,
                           self.seventytoeighty1,
                           self.eightytoninety1, self.ninetytohundred1, round(coverage2, 2), self.amount_seq)
        print(report)

        # maakt allemaal verschillende plotjes door middel van r scripts.
        pipeline.report_output_file(report, "Info_about_consensus.txt")
        pipeline.frequency_coverage()
        pipeline.frequency_coverage_without_gaps()
        pipeline.mutation_msa_plot()
        pipeline.mutation_consensus_plot()
        pipeline.coverage_per_pos_plot_no_gaps()
        pipeline.coverage_per_pos_plot_gaps()

        report = "The MSA allows {}% gaps on one position\n mutant sequences position are positions on de consensus" \
                 "where the consensus is 60% or less certain that it is the correct nucleotide on that position, or " \
                 "when it is les than 80% certain and are three or more nucleotides spotted on that position. \n" \
                 "mutational regio's are seperated when a mutation is more than 20 position away from the previous " \
                 "one.\n There are {} presumable mutation counted" \
                 "" \
            .format((1 - self.threshold) * 100, self.mut_count)
        print(report)
        pipeline.report_output_file(report, "mutant_sequences.txt")

    def calculating_consensus(self, pipeline, pos, percentage):
        nuc_key = {
            "M": random.choice("AC"), "R": random.choice("AG"), "W": random.choice("A{}".format(self.types)),
            "S": random.choice("CG"),
            "Y": random.choice("C{}".format(self.types)), "K": random.choice("G{}".format(self.types)), "V": random.choice("ACG"),
            "H": random.choice("AC{}".format(self.types)),
            "D": random.choice("AG{}".format(self.types)), "B": random.choice("CG{}".format(self.types)), "X": random.choice("GA{}C".format(self.types)),
            "N": random.choice("GA{}C".format(self.types))
        }

        keys = list(nuc_key.keys())

        try:
            self.letter = max(self.res.keys(), key=(lambda k: self.res[k]))
            listofkeys = [key for (key, value) in self.res.items() if value == self.res["{}".format(self.letter)]]
            """
            Kijkt of de meest voorkomden letter een - is. als dat zo is dan wordt deze verwijderd en wordt er
            daarna gekeken of de meest voorkomende een non-nucleotde letter is. Als dat zo is dan wordt er niks
            in de consensus sequentie geplaats. Als het niet zo is en de hoogste wordt maar 1 keer geteld dan
            wordt er ook niks in de sequentie geplaats. Als het niet zo is en de hoogste wordt vaker dan 1 keer
            geteld dan wordt deze letter gebruikt voor de consensus sequentie.
            """
            pipeline.calculate_threshold(pos)
            if self.letter[0] == "-" or "-{}".format(pos) in listofkeys:
                pipeline.highest_gaps(pos)

            self.letter = max(self.res.keys(), key=(lambda k: self.res[k]))
            listofkeys = [key for (key, value) in self.res.items() if value == self.res["{}".format(self.letter)]]

            # hier pakt hij de nucleotiden die elke sequentie heeft gekozen voor die plek.
            # daarnaast maakt kijkt hij of ze minimaal 5 nucleatide achtereenvolgend perfect zijn gevonden.
            # en schrijft het naar het raport toe.
            # de variabelen kun je aanpassen als je bijv een langere opeenvolgende seq wilt hebben, of
            # het niet alle sequentie de nucleotide op die plek te kiezen maar een zelf meegegeven aantal of
            # meer.

            # als de meest voorkomende letter evenvaak of meer voorkomt dan zelf meegegeven procent.

            # als de meest getelde letter maar 1 keer geteld wordt dan wordt er niks in de sequentie neergezet.
            if self.res["{}".format(self.letter)] == 1 or self.counted_nuc_on_pos < percentage * self.threshold:
                self.consensus += ""
                self.corrected_pos += 1
            else:
                """
                Als de meest voorkomend op deze positie een non-nucleotide is dat 3 of meer nucleotide
                representeerd dan wordt deze verwijderd. Als de meest komende dan een - is dan wordt deze ook
                verwijderd.
                """
                list1 = [a for a in "VHDNBX-"]
                while all(item in list1 for item in listofkeys):
                    for compare in "VHDNBX-":
                        while "{}{}".format(compare, pos) in listofkeys:
                            print("deleting {} op pos {}".format(self.letter[0], pos))

                            del self.res["{}{}".format(self.letter[0], pos)]

                            self.letter = max(self.res.keys(), key=(lambda k: self.res[k]))

                            if self.letter[0] == "-" or "-{}".format(pos) in listofkeys:
                                pipeline.highest_gaps(pos)

                            self.letter = max(self.res.keys(), key=(lambda k: self.res[k]))

                            listofkeys = [key for (key, value) in self.res.items() if
                                          value == self.res["{}".format(self.letter)]]

                """
                Als de nucleotide evenvaak of vaker dan de meegegeven percentage per positie wordt waargenomen, 
                dan wordt er gekeken naar de positie van de vorige keer dat dit gebeurde doormiddel van het 
                verschil van positie van de vorige waarneming met de huidige waarneming te vergelijken. Als dit
                1 is dan betekent het dus dat dit de tweede achter elkaar is en wordt de eerste positie van deze
                waarnemingen genoteerd. Als er meer dan 4 nucleotide achter elkaar aan de eisen van minimale 
                percentage geteld voldoen, dan wordt dit in een rapportje gezet en naar de file 
                correct_sequences.txt gezet.
                """
                if self.res["{}{}".format(self.letter[0], pos)] >= percentage * (1 - (100 - int(self.counts)) / 100):
                    pipeline.calculate_perfect(pos, pipeline)
                ####################

                ####################
                # lijst met de letter die de hoogste waarde hebben.
                """
                Als - in de lijst met meest getelde letter zitten dan moet hij eruit worden gehaald. Omdat er
                geen gaps in de consensus mogen zitten.
                """

                # kijkt of er meerdere letters zijn met de hoogst getelde waardes
                if len(listofkeys) > 1:
                    let = pipeline.check_the_values(pos, nuc_key, pipeline, listofkeys, self.letter)

                    # als de letter veranderd is nadat hij door de check_the_values functie is geweest
                    # print het naar de terminal en schrijf het naar een report file.
                    if self.letter is not let:
                        report = "{} voordat er gekeken werd naar non-nucleotides characters, {} erna, op " \
                                 "positie {}".format(self.letter[0], let[0], (pos - self.corrected_pos)) + "\n"
                        print(report)
                        pipeline.report_output_file(report, "Nuc_changes.txt")

                    elif self.letter is let:
                        report = "letter veranderde niet voor positie {}. Nadat er werdt gekeken naar de " \
                                 "niet-nucleotide characters.".format((pos - self.corrected_pos)) + "\n"
                        print(report)
                        pipeline.report_output_file(report, "Nuc_changes.txt")

                    self.letter = let
                    self.consensus += self.letter[0]

                else:
                    # als de letter een vreemde letter is verander hem dan naar A,C,T of G
                    if self.letter[0] in keys and len(listofkeys) == 1:
                        self.consensus += ""
                        self.corrected_pos += 1
                    else:
                        self.consensus += self.letter[0]

                number_of_nucleotide = [e[0] for e in list(self.res.keys()) if e[0] in "AC{}G".format(self.types)]
                number_of_nucleotides = len(number_of_nucleotide)

                # als het aantal tussen 10 en 60% zit. Dan is er waarschijnlijk een mutatie op deze positie.
                # wanneer er minder zijn dan 70% maar er 3 verschillende nucleotide waargenomen zijn is het een mutatie.
                if self.counted_nuc_on_pos * 0.1 < self.res["{}".format(self.letter)] <= self.counted_nuc_on_pos * 0.6:
                    pipeline.calculate_mutation(pos, pipeline, number_of_nucleotides, number_of_nucleotide)
                elif self.res["{}".format(self.letter)] <= self.counted_nuc_on_pos * 0.7:
                    if number_of_nucleotides >= 3:
                        pipeline.calculate_mutation(pos, pipeline, number_of_nucleotides, number_of_nucleotide)

                pipeline.percentages_with_gaps(pos, percentage, pipeline)
                pipeline.percentages_without_gaps(pos, pipeline)

        except ValueError:
            # dit is om de laatste lege waardes op te vangen en om de positie te kunnen corrigeren.
            print("placing nothing on pos {}".format(pos))
            self.consensus += ""
            self.corrected_pos += 1

    def percentages_with_gaps(self, pos, percentage, pipeline):
        try:
            """
            Hier worden verschillende percentages coverage van de consensus bepaald en genoteerd.
            """
            report = "{},{}\n".format(pos - self.corrected_pos, self.res["{}".format(self.letter)] / percentage * 100)
            pipeline.report_output_file(report, "coverage_per_pos_consensus_gaps")

            if self.letter[0] in "AC{}G".format(self.types):
                if 1 < self.res["{}".format(self.letter)] <= percentage * 0.1:
                    self.zerototen += 1
                elif percentage * 0.1 < self.res["{}".format(self.letter)] <= percentage * 0.2:
                    self.tentotwenty += 1
                elif percentage * 0.2 < self.res["{}".format(self.letter)] <= percentage * 0.3:
                    self.twentytothirty += 1
                elif percentage * 0.3 < self.res["{}".format(self.letter)] <= percentage * 0.4:
                    self.thirtytofourthy += 1
                elif percentage * 0.4 < self.res["{}".format(self.letter)] <= percentage * 0.5:
                    self.fourthytofifty += 1
                elif percentage * 0.5 < self.res["{}".format(self.letter)] <= percentage * 0.6:
                    self.fiftytosixty += 1
                elif percentage * 0.6 < self.res["{}".format(self.letter)] <= percentage * 0.7:
                    self.sixtytoseventy += 1
                elif percentage * 0.7 < self.res["{}".format(self.letter)] <= percentage * 0.8:
                    self.seventytoeighty += 1
                elif percentage * 0.8 < self.res["{}".format(self.letter)] <= percentage * 0.9:
                    self.eightytoninety += 1
                elif percentage * 0.9 < self.res["{}".format(self.letter)] <= percentage * 1:
                    self.ninetytohundred += 1

        except KeyError:
            print("last nucleotides are -")

    def percentages_without_gaps(self, pos, pipeline):
        try:
            """
            Hier worden verschillende percentages coverage van de consensus bepaald en genoteerd.
            """

            if self.letter[0] in "AC{}G".format(self.types):

                report = "{},{}\n".format(pos - self.corrected_pos, self.res["{}".format(self.letter)]/ self.counted_nuc_on_pos * 100)
                pipeline.report_output_file(report, "coverage_per_pos_consensus")

                if 1 < self.res["{}".format(self.letter)] <= self.counted_nuc_on_pos * 0.1:
                    self.zerototen1 += 1
                elif self.counted_nuc_on_pos * 0.1 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.2:
                    self.tentotwenty1 += 1
                elif self.counted_nuc_on_pos * 0.2 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.3:
                    self.twentytothirty1 += 1
                elif self.counted_nuc_on_pos * 0.3 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.4:
                    self.thirtytofourthy1 += 1
                elif self.counted_nuc_on_pos * 0.4 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.5:
                    self.fourthytofifty1 += 1
                elif self.counted_nuc_on_pos * 0.5 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.6:
                    self.fiftytosixty1 += 1
                elif self.counted_nuc_on_pos * 0.6 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.7:
                    self.sixtytoseventy1 += 1
                elif self.counted_nuc_on_pos * 0.7 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.8:
                    self.seventytoeighty1 += 1
                elif self.counted_nuc_on_pos * 0.8 < self.res["{}".format(self.letter)] \
                        <= self.counted_nuc_on_pos * 0.9:
                    self.eightytoninety1 += 1
                elif self.counted_nuc_on_pos * 0.9 < self.res["{}".format(self.letter)] <= self.counted_nuc_on_pos * 1:
                    self.ninetytohundred1 += 1

        except KeyError:
            print("last nucleotides are -")

    def calculate_mutation(self, pos, pipeline, number_of_nucleotides, number_of_nucleotide):
        """"
        berekend of er mogelijke mutaties per positie te vinden zijn.

        """

        self.mut_count += 1
        self.mutant_called += 1
        self.mutant_seq += self.letter[0]
        self.mutation_pos_consensus.append(pos - self.corrected_pos)
        self.mutation_pos_msa.append(pos)

        report = "{},{},{},{},red\n".format(pos, pos - self.corrected_pos, 1, self.mutatie_regios)
        pipeline.report_output_file(report, "mutations")

        apart = 50

        # kijkt of het verschil van de positie van de perfect called 1 is.
        if (pos - self.post_mut) == 1:
            self.first_pos_mut = pos - self.mutant_called
            ratio_nuc = pipeline.make_ratio_string(pipeline, pos, number_of_nucleotide)
            report = "\nThere are {} different nucleotides here in a ratio of {} on pos {}\n" \
                .format(number_of_nucleotides, ratio_nuc, pos - self.corrected_pos)
            print(report)
            pipeline.report_output_file(report, "mutant_sequences.txt")

        # als de laatste mutatie meer dan 50 posities terug is. dan betekent dat het einde van deze mutatie regio.
        elif (pos - self.post_mut) > apart:
            report = "\n\n\n" \
                     "_________________________________________________________" \
                     "previous mutation is {} position away from the current one. Thus ending a 'mutational region'" \
                     "{}" \
                     "_________________________________________________________" \
                     "\n\n\n".format(apart, self.mutatie_regios)
            pipeline.report_output_file(report, "mutant_sequences.txt")

            self.first_pos_mut = pos - self.mutant_called
            ratio_nuc = pipeline.make_ratio_string(pipeline, pos, number_of_nucleotide)
            report = "There are {} different nucleotides here in a ratio of {} on pos {}\n" \
                .format(number_of_nucleotides, ratio_nuc, pos - self.corrected_pos)
            print(report)
            pipeline.report_output_file(report, "mutant_sequences.txt")
            self.mutant_called = 0
            self.mutant_seq = ""
            self.mutatie_regios += 1

        # zet de lengte en sequentie van perfect op nul
        else:
            # als de opeenvolgende langer is dan zelf meegegeven minimum lengte
            # schrijf het naar een report
            if self.mutant_called >= 2:
                ratio_nuc = pipeline.make_ratio_string(pipeline, pos, number_of_nucleotide)
                report = "There are {} different nucleotides here in a ratio of {} on pos {}\n" \
                    .format(number_of_nucleotides, ratio_nuc, pos - self.corrected_pos)
                print(report)
                pipeline.report_output_file(report, "mutant_sequences.txt")

                report = "Between {} and {} has {} consecutive mutant basecalled the seq = {}\n\n" \
                    .format((self.first_pos_mut - self.corrected_pos + 1), (self.post_mut - self.corrected_pos + 1),
                            self.mutant_called,
                            self.mutant_seq)
                print(report)
                pipeline.report_output_file(report, "mutant_sequences.txt")
                self.mutant_called = 0
                self.mutant_seq = ""

            else:
                ratio_nuc = pipeline.make_ratio_string(pipeline, pos, number_of_nucleotide)

                report = "There are {} different nucleotides here in a ratio of {} on pos {}\n" \
                    .format(number_of_nucleotides, ratio_nuc, pos - self.corrected_pos)
                print(report)
                pipeline.report_output_file(report, "mutant_sequences.txt")

                self.mutant_called = 0
                self.mutant_seq = ""

        self.post_mut = pos
        return 0

    def make_ratio_string(self, pipeline, pos, number_of_nucleotide):
        ratio_nuc = ""
        for char in number_of_nucleotide:
            if char == "A":
                ratio_nuc += "A:{}%".format(pipeline.calculate_ratio(pos, "A"))
            elif char == "C":
                ratio_nuc += "C:{}%".format(pipeline.calculate_ratio(pos, "C"))
            elif char == "{}".format(self.types):
                ratio_nuc += "{}:{}%".format(self.types, pipeline.calculate_ratio(pos, "{}".format(self.types)))
            elif char == "G":
                ratio_nuc += "G:{}%".format(pipeline.calculate_ratio(pos, "G"))
            ratio_nuc += ", "
        ratio_nuc = ratio_nuc[:-2]
        return ratio_nuc

    def calculate_ratio(self, pos, char):
        """
        berekend het ratio van coverage op deze positie
        :param pos: positie op het MSA
        :param char: nucleotide
        :return: ratio
        """
        ratio = 0
        try:
            ratio = round(self.res["{}{}".format(char, pos)] / self.counted_nuc_on_pos * 100, 2)
        except KeyError:
            print("{} not in res".format(char))

        return ratio

    def calculate_threshold(self, pos):
        """
        makes a threshold
        :param pos:
        :return:
        """
        if "-{}".format(pos) in self.res:
            self.counted_nuc_on_pos = sum(self.res.values()) - self.res["-{}".format(pos)]
        else:
            self.counted_nuc_on_pos = sum(self.res.values())

    def calculate_perfect(self, pos, pipeline):
        self.perf_count += 1
        self.perfect_called += 1
        self.perfect_seq += self.letter[0]
        # kijkt of het verschil van de positie van de perfect called 1 is.
        if (pos - self.post) == 1:
            self.first_pos = pos - self.perfect_called

        # zet de lengte en sequentie van perfect op nul
        else:
            # als de opeenvolgende langer is dan zelf meegegeven minimum lengte
            # schrijf het naar een report
            if self.perfect_called >= int(self.perfectlength):
                report = "between {} and {} has {} consecutive perfect basecalled the seq = {}\n" \
                    .format((self.first_pos - self.corrected_pos + 1), (self.post - self.corrected_pos + 1),
                            self.perfect_called,
                            self.perfect_seq)
                print(report)
                pipeline.report_output_file(report, "correct_sequences.txt")
            self.perfect_called = 0
            self.perfect_seq = ""
        self.post = pos

    def check_the_values(self, pos, nuc_key, pipeline, listofkeys, letter):
        """
        de functie zorgt ervoor dat er 1 letter met de hoogste value gekozen wordt door eerst te kijken of een van
        de vreemde letters een van de meerdere hoogste nucleotide kan zijn. Zo ja dan wordt die returned. zo niet
        of kan die vreemde letters alle hoogste waardes zijn. Dan wordt er een random nucleotide gekozen.
        :param letter: de letter/pos met de hoogst voorkomende count
        :param pos: positie op het moment dat de functie wordt uitgevoerd.
        :param nuc_key: een dictionary met random letters.
        :param pipeline: class object om report_output_file functie van de classe consensus te gebruiken.
        :param listofkeys: geeft een lijst met de meest getelde nucleotide mee.
        :return: de letter met de hoogste count value
        """
        rest = self.res
        nuc_keys = {
            "M": "AC", "R": "AG", "W": "A{}".format(self.types),
            "S": "CG",
            "Y": "C{}".format(self.types), "K": "G{}".format(self.types), "V": "ACG",
            "H": "AC{}".format(self.types),
            "D": "AG{}".format(self.types), "B": "CG{}".format(self.types), "X": "GA{}C".format(self.types),
            "N": "A{}C".format(self.types)
        }
        # keys met dezelfde hoge waarde

        # lijst met keys van vreemde letters
        key = list(nuc_key.keys())

        try:
            # als er meer dan 1 in de lijst van keys met hoge waardes zit ga dan door.
            # voor elke letter in de lijst met hoge waardes
            # print("meer dan 1 hoogste waarde")
            sequentie = ""
            vreemde = []
            # zet de vreemde letter van de gemaakte dictionary op de positie in een lijst
            for vreemd in list(self.res.keys()):
                if vreemd[0] in key:
                    vreemde.append(vreemd[0])
            listofkeys2 = []

            # als er vreemde letters in de lijst vreemde zitten.
            if len(vreemde) > 0:
                # voor elke letter in de lijst met letter met dezelfde hoogste waardes
                for letters in listofkeys:
                    # voor elke vreemde letter in lijst van vreemde letters
                    for ambiguity in vreemde:
                        # als de letters van hoogste waardes in de seq matched met de value die het kan zijn door te
                        # in de dictionary nuc_keys met de letters van de lijst met vreemde als keys. Dan wordt krijgt
                        # die letter een extra count.
                        if letters[0] in nuc_keys["{}".format(ambiguity)]:
                            rest["{}".format(letters)] += 1
                            sequentie += letters[0]
                            letter = max(rest.keys(), key=(lambda k: rest[k]))
                            listofkeys2 = [key for (key, value) in rest.items() if
                                           value == rest["{}".format(letter)]]

                # als er nog steeds twee of meer hoogste waardes/letters zijn.
                # kies een random letter en schrijf het naar een report
                if len(listofkeys2) > 1:
                    print(sequentie)
                    sequentie = "".join(set(sequentie))
                    a = random.choice("{}".format(sequentie))
                    rest["{}{}".format(a, pos)] += 1
                    letter = max(rest.keys(), key=(lambda k: rest[k]))
                    report = "er moet nog steeds gekozen worden tussen deze letters: {} voor deze positie: {}".format(
                        sequentie, (pos - self.corrected_pos)) + "\n"
                    print(report)
                    pipeline.report_output_file(report, "Nuc_changes.txt")
                # als er nu wel maar 1 hoogste waardes is schrijf hem naar een reportje
                if len(listofkeys2) == 1:
                    report = "dit is hem geworden na het kijken van een non-nucleotide bij een positie waarbij meer " \
                             "dan 1 nucleotide de hoogste waarde heeft:{} op positie:{}" \
                                 .format(letter[0], (pos - self.corrected_pos)) + "\n"
                    print(report)
                    pipeline.report_output_file(report, "Nuc_changes.txt")
            else:
                letter = letter

        except ValueError:
            print('error')
        return letter

    def report_output_file(self, report, name):
        """

        :param report: Is de zin die aan de file wordt toegevoegd
        :param name: Zegt in welke file hij moet schrijven.
        :return:
        """
        # kijkt of de file al is aangemaakt. zo ja dan voegt hij de report als zin eraan toe,
        # zo niet dan maakt hij de file aan en voegt de report eraan toe.
        if os.path.isfile("{}{}".format(self.output_dir, name)):
            with open("{}{}".format(self.output_dir, name), 'a') as file:
                file.write(report)
                file.close()
        else:
            with open("{}{}".format(self.output_dir, name), "w+") as file:
                file.write(report)
                file.close()

    def frequency_coverage(self):
        os.system(
            "Rscript quality_test.R {}frequency_coverage.jpeg {} {} {} {} {} {} {} {} {} {} '{}'".format(self.output_dir,
                                                                                                self.zerototen,
                                                                                                self.tentotwenty,
                                                                                                self.twentytothirty,
                                                                                                self.thirtytofourthy,
                                                                                                self.fourthytofifty,
                                                                                                self.fiftytosixty,
                                                                                                self.sixtytoseventy,
                                                                                                self.seventytoeighty,
                                                                                                self.eightytoninety,
                                                                                                self.ninetytohundred,
                                                                                                "Nucleotide frequency "
                                                                                                "per % coverage "
                                                                                                "compared"
                                                                                                " to all sequences \n "
                                                                                                "in MSA"))

    def frequency_coverage_without_gaps(self):
        os.system(
            "Rscript quality_test.R {}frequency_coverage_without_gaps.jpeg {} {} {} {} {} {} {} {} {} {} '{}'".format(self.output_dir,
                                                                                                self.zerototen1,
                                                                                                self.tentotwenty1,
                                                                                                self.twentytothirty1,
                                                                                                self.thirtytofourthy1,
                                                                                                self.fourthytofifty1,
                                                                                                self.fiftytosixty1,
                                                                                                self.sixtytoseventy1,
                                                                                                self.seventytoeighty1,
                                                                                                self.eightytoninety1,
                                                                                                self.ninetytohundred1,
                                                                                                "Nucleotide frequency "
                                                                                                "per % coverage "
                                                                                                "compared to all "
                                                                                                "sequences \n in MSA "
                                                                                                "minus counted gaps on "
                                                                                                "that position"))

    def mutation_msa_plot(self):
        os.system(
            "Rscript msa_mutation.R {}mutations {}Mutations_msa.jpeg {}".format(self.output_dir, self.output_dir,
                                                                                self.sequence_length))
        return 0

    def mutation_consensus_plot(self):
        print(len(self.consensus))
        os.system(
            "Rscript consensus_mutation.R {}mutations {}Mutations_consensus.jpeg {}".format(self.output_dir,
                                                                                            self.output_dir,
                                                                                            len(self.consensus)))
        return 0

    def coverage_per_pos_plot_gaps(self):
        os.system(
            "Rscript coverage_per_pos_plot_gaps.R "
            "{}coverage_per_pos_consensus_gaps {}coverage_per_pos_consensus_gaps.jpeg".format(self.output_dir,
                                                                                            self.output_dir))
        return 0

    def coverage_per_pos_plot_no_gaps(self):
        os.system(
            "Rscript coverage_per_pos_plot_no_gaps.R "
            "{}coverage_per_pos_consensus {}coverage_per_pos_consensus.jpeg".format(self.output_dir,
                                                                                            self.output_dir))
        return 0

def argparser():
    """
    hier kun je de input file naam, output file naam, report file naam, lengte van accurate sequentie en het aantal
    percentage van sequentie dat dezelfde nucleotide had gekozen.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--inputfile", default=input_file,
                        help="insert the filename of the input file must be a msa output .fasta file. Example: "
                             " /0_iteration/msa_output.fa")
    parser.add_argument("--outputfile", default=output_file,
                        help="insert the filename of the output file. It is a fasta so must end with .fa or .fasta"
                             " Example: /0_iteration/Your_own_consensus.fasta")
    parser.add_argument("--reportdir", default=report_dir,
                        help="insert the directory for the report files."
                             " Example: /0_iteration/")

    parser.add_argument("--perfectlength", default=20,
                        help="insert the minimal length of the sequence for your report file for consecutive (100 percent)self "
                             "given correct nucleotide counts.")

    parser.add_argument("--counts", default=100,
                        help="insert percentage in numbers (0-100) for how many you want to be correct to be called for in your "
                             "report. Must be higher than 50 percent")

    parser.add_argument("--type", default="dna",
                        help="was the msa of dna or rna?")

    parser.add_argument("--header", default="default_header",
                        help="header for consensus fasta file")

    parser.add_argument("--maximum_gaps", default=0.2,
                        help="put in the maximum percentage of gaps that you will allow. between 0.0 and 1.0")

    args = parser.parse_args()
    return args


def main():
    """
    Voert alles op volgorde uit en schrijft naar de Info_bout_consenus file hoelang het duurde om alles te hebben
    uitgevoerd.g
    :return:
    """
    # uitvoeren

    start = time.time()
    pipe_line = Consensus()
    pipe_line.check_files()
    seq = pipe_line.sequence_parser()
    direc = pipe_line.nuc_count(seq, pipe_line)
    pipe_line.write_consensus(seq, direc, pipe_line)

    end = time.time()
    react_time = end - start
    reports = "\n\nIt took the program {} seconds to complete.".format(round(react_time))
    pipe_line.report_output_file(reports, "Info_about_consensus.txt")
    print("It took the program {} seconds to complete.".format(round(react_time)))

    return 0


if __name__ == '__main__':
    sys.exit(main())
